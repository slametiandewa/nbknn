package classificationckd;

import weka.core.Instances;
import java.util.Random;
import weka.core.converters.ConverterUtils.DataSource;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.lazy.IBk;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Utils;

/**
 *
 * @author Slametian Dewa Tegar Perkasa
 */
public class ClassificationCKD {
    
    public static void Klasifikasi(String namafile) throws Exception {
        DataSource source = new DataSource("D:/Universitas Negeri Malang/Semester 8/Skripsi/HASIL PREPROCESSING/Kombinasi Atribut/" + namafile + ".arff");
        Instances dataset = source.getDataSet();
        
        dataset.setClassIndex(dataset.numAttributes()-1);
        
        long timeBuildModel = 0, trainTime = 0;
        
        trainTime = System.currentTimeMillis();
        
        Classifier[] classifiers = {new NaiveBayes(), new IBk(3)};
        
        for(Classifier c : classifiers) {
            Classifier cls = c;
            cls.buildClassifier(dataset);
            Evaluation eval = new Evaluation(dataset);
            Random rand = new Random(1);
            int folds = 10;
        
            eval.crossValidateModel(cls, dataset, folds, rand);
            timeBuildModel = System.currentTimeMillis() - trainTime;
            System.out.println("Time taken to build model: " + Utils.doubleToString(timeBuildModel/1000.0, 4) + " seconds\n");
            
            System.out.println(eval.toSummaryString("Evaluation Results:\n", false));
            System.out.println(eval.toClassDetailsString());
            System.out.println("Accuracy = " + eval.pctCorrect() + "%");
            System.out.println("Precision = " + (eval.precision(0)* 100) + "%");
            System.out.println("Recall = " + (eval.recall(0) * 100) + "%");
            System.out.println("----------------------------------------\n");
        }
    }
    
    public static void main(String[] args) throws Exception {
        System.out.println("EKSPERIMEN 1");
        Klasifikasi("Eksperimen1");
        System.out.println("EKSPERIMEN 2");
        Klasifikasi("Eksperimen2");
        
    }
}
